# CHANGELOG #

## 0.4.3 ##

  + Include `amsmath`, `amssymb` and `todonotes` as dependencies.
  + Include the `\mytodo` command for appealing todo notes while drafting.

## 0.4.2 ##

  * The command `\bmath` for nicer mathematical expressions.
  * Changed the background colour for `\bverb`.

## 0.4.1 ##

  * The command `\timelineevent` which can be used to create a time line. An example is given in `demo.tex`

## 0.4.0 ##

  * Use the `xcolor` and `newverb` package to create `\bverb` which provides nicer verbatim text.
