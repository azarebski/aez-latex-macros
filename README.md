The file `aezmacros.sty` provides a package `aezmacros`. This is a constant work
in progress so if you use it, make sure to check which version you have! To use
the commands in your latex document just add `\usepackage{aezmacros}` in the
preamble and ensure that the `aezmacros.sty` file is in the directory of your
`tex` files. See `demo.tex` for examples of the macros provided. To compile a
PDF use `pdflatex demo`.
